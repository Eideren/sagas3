namespace SAGAS3
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Avalonia;
    using Avalonia.Controls;
    using Avalonia.Layout;



    public class IndexerPathPanel : MainWindowPanel
    {
        readonly Canvas _canvas;
        
        
        TaskCompletionSource<string[]> _taskCompletionSource = new TaskCompletionSource<string[]>();

        protected override Task Job => _taskCompletionSource.Task;
        protected override Canvas Canvas => _canvas;
        

        public static Task<string[]> AsModal( string[] pathsOption, MainWindow window )
        {
            return new IndexerPathPanel( window, pathsOption )._taskCompletionSource.Task;
        }
        
        
        

        IndexerPathPanel( MainWindow window, string[] pathsOption ) : base( window )
        {
            var selectedPaths = new HashSet<string>( pathsOption );
            LogicTheme logicTheme = window.LogicTheme;
            
            var scan = new Button
            {
                Width = (window.Width - ( logicTheme.Margin.Left + logicTheme.Margin.Right )) / 2d,
                Height = 28d,
                Margin = logicTheme.Margin,
                BorderBrush = null,
                Padding = new Thickness( 0d ),
                Background = logicTheme.Green,
                Content = new TextBlock
                {
                    Text = "Scan",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                },
            };
            scan.Click += ( s, e ) =>
            {
                _taskCompletionSource.TrySetResult( selectedPaths.ToArray() );
            };
            scan.ImplementHighlight();
            Canvas.SetBottom( scan, 0 );
            Canvas.SetLeft( scan, 0 );
            
            
            
            var cancel = new Button
            {
                Width = scan.Width,
                Height = scan.Height,
                Margin = scan.Margin,
                BorderBrush = scan.BorderBrush,
                Padding = scan.Padding,
                Background = logicTheme.Red,
                Content = new TextBlock
                {
                    Text = "Cancel",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                }
            };
            cancel.Click += ( s, e ) =>
            {
                _taskCompletionSource.TrySetResult( System.Array.Empty<string>() );
            };
            cancel.ImplementHighlight();
            Canvas.SetBottom( cancel, 0 );
            Canvas.SetRight( cancel, 0 );


            
            CheckBox[] checkboxes = new CheckBox[ pathsOption.Length ];
            for( int i = 0; i < checkboxes.Length; i++ )
            {
                var path = pathsOption[ i ];
                var checkBox = new CheckBox()
                {
                    BorderThickness = new Thickness( 2 ),
                    IsChecked = selectedPaths.Contains( path ),
                    Margin = new Thickness( logicTheme.Margin.Left, 0 ),
                    Content = new TextBlock
                    {
                        Text = path,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center,
                    }
                };
                checkBox.Click += ( s, a ) => 
                {
                    if( checkBox.IsChecked == true || checkBox.IsChecked == null )
                    {
                        if( selectedPaths.Contains( path ) == false )
                            selectedPaths.Add( path );
                    }
                    else
                    {
                        if( selectedPaths.Contains( path ) )
                            selectedPaths.Remove( path );
                    }
                };
                checkboxes[ i ] = checkBox;
            }
            // Margin on first and last
            if( checkboxes.Length > 0 )
            {
                checkboxes[ 0 ].Margin += new Thickness( 0, logicTheme.Margin.Top, 0, 0 );
                checkboxes[ ^1 ].Margin += new Thickness( 0, 0, 0, logicTheme.Margin.Bottom );
            }
            
            
            var checkboxPanel = new StackPanel
            {
                Width = MainWindow.Width - ( logicTheme.Margin.Left + logicTheme.Margin.Right ),
                Orientation = Orientation.Vertical,
            };
            checkboxPanel.Children.AddRange( checkboxes );
            
            _canvas = new Canvas
            {
                Width = MainWindow.Width,
                Height = MainWindow.Height,
                Children =
                {
                    new StackPanel
                    {
                        Orientation = Orientation.Vertical,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center,
                        Spacing = logicTheme.Margin.Bottom,
                        MinHeight = MainWindow.Height,
                        Margin = logicTheme.Margin,
                        Children =
                        {
                            new TextBlock
                            {
                                Text = "Select drives to scan",
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                Margin = new Thickness( logicTheme.Margin.Left, logicTheme.Margin.Top, logicTheme.Margin.Right, 0 ),
                            },
                            checkboxPanel,
                        }
                    },
                    scan,
                    cancel
                }
            };
        }
    }
}