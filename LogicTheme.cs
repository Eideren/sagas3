﻿namespace SAGAS3
{
	using Avalonia;
	using Avalonia.Media;



	public class LogicTheme
	{
		public static LogicTheme Default => new LogicTheme();


		public readonly SolidColorBrush Background;
		public readonly SolidColorBrush Red;
		public readonly SolidColorBrush Green;
		public readonly SolidColorBrush Blue;
		public readonly IBrush Transparent;
		public readonly Thickness Margin;
		public readonly Thickness Border;
		public readonly double ListItemHeight;
		


		LogicTheme()
		{
			Background = new SolidColorBrush { Color = Color.FromRgb( 62, 62, 64 ) };
			Red = new SolidColorBrush { Color = Color.FromRgb( 194, 87, 87 ) };
			Green = new SolidColorBrush { Color = Color.FromRgb( 89, 206, 89 ) };
			Blue = new SolidColorBrush { Color = Color.FromRgb( 89, 89, 206 ) };
			Transparent = new SolidColorBrush { Opacity = 0d };
			Margin = new Thickness( 10d, 8d );
			Border = new Thickness( 0d );
			ListItemHeight = 28d;
		}
	}
}