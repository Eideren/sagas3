﻿namespace SAGAS3
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Threading;
	using System.Threading.Tasks;
	using System.Xml;
	using System.Xml.Linq;



	public class Indexer
	{
		static readonly TimeSpan WRITE_TO_FILE_DELAY = TimeSpan.FromSeconds( 3 );
		static readonly string DEFAULT_FILE_PATH = Path.Combine( Path.GetDirectoryName( Assembly.GetEntryAssembly().Location ), "Indexer.xml" );

		const string XmlContainer = "IndexerContent";
		const string XmlElement = "E";
		const string XmlPath = "P";
		const string XmlScore = "S";
		
		static Indexer()
		{
			// Static constructors are thread safe
			Instance = new Indexer( DEFAULT_FILE_PATH );
			// Load from disk
		}
		
		public static readonly Indexer Instance;

		public readonly Task Loading;
		public long LastChangeTimeStamp{ get; private set; } = uint.MinValue;

		readonly Dictionary<string, int> _dictionary = new Dictionary<string, int>();
		readonly Dictionary<string, (int amountOfPath, FileSystemWatcher watcher)> _dirWatcher = new Dictionary<string, (int, FileSystemWatcher)>();
		
		readonly object _schedulerLock = new object();
		readonly object _writingToFileLock = new object();
		long _lastWriteTS = uint.MinValue;
		bool _saveScheduled;
		readonly string _IOPath;



		Indexer( string ioPath )
		{
			_IOPath = ioPath;
			Loading = Task.Run(() =>
			{
				try
				{
					Instance.AppendFromFile(_IOPath);
				}
				catch (Exception e)
				{
					Program.SendExceptionToMessageBox(e);
				}
			});
		}





		public (string path, string filename, double score)[] Search( string searchTerm, CancellationToken token, bool caseSensitive )
		{
			KeyValuePair<string, int>[] paths;
			lock( _dictionary )
			{
				paths = _dictionary.ToArray();
			}

            (string path, string fileName, double score)[] scores = new (string, string, double)[ paths.Length ];

            bool purelyScoreBased = string.IsNullOrWhiteSpace( searchTerm );
            
            int jobCount = Environment.ProcessorCount;
            int amountPerJob = scores.Length / jobCount;
            // Split job per core instead of per string, this will reduce the time wasted by calling the virtual function
            Parallel.For( 0, jobCount, jobIndex =>
            {
                int start = jobIndex * amountPerJob;
                int end = jobIndex == jobCount - 1 ? scores.Length : start + amountPerJob;
                for( int i = start; i < end; i++ )
                {
                    if( token.IsCancellationRequested )
                        return;
                    
                    string value = paths[ i ].Key;
                    int baseExecCount = paths[ i ].Value;
                    string fileName = Path.GetFileNameWithoutExtension( value );
                    double score;
                    if( purelyScoreBased == false )
                    {
	                    score = FuzzyMatchScore( searchTerm, fileName );
		                    

	                    { // Increase score based on amount of executions
		                    
		                    // The increase is non-linear, each additional execution does increase the score
		                    // but by a smaller amount than the previous one so that extremely high amount of
		                    // executions don't break the string match score.
		                    
							// Avoid zeroes and below
		                    double mult = Math.Min( baseExecCount + 1d, 1d );
		                    // 1 -> 1, 2->1.189207115, 3->1.31607401295
		                    mult = Math.Pow( mult, 0.25d );
		                    // 1 -> 1, 1.189207115 -> 0.84089641525, 1.31607401295 -> 0.75983568565
		                    mult = 1d / mult;
		                    // 1 -> 0, 0.84089641525 -> 0.15910358475, 0.75983568565 -> 0.24016431435
		                    mult = ( 1d - mult );
		                    // mult will never go past 1.0, so the score will at most be doubled
		                    score += score * mult;
	                    }
                    }
                    else
                    {
	                    score = baseExecCount;
                    }
                    scores[ i ] = ( value, fileName, score );
                }
            } );
            
            if( token.IsCancellationRequested )
                return new (string, string, double)[0];
            
            return scores.AsParallel().OrderByDescending( s => s.score ).ToArray();
		}


		
		double FuzzyMatchScore( string term, string against )
		{
			double scoreInOrder = 0;
			int jCarret = 0;
			for( int i = 0; i < term.Length; i++ )
			{
				for( int j = jCarret; j < against.Length; j++ )
				{
					bool match = term[ i ] == against[ j ];
					bool anyMatch = match || char.ToLower( term[ i ] ) == char.ToLower( against[ j ] );
					if( anyMatch )
					{
						double baseScore = match ? 1d : 0.5d;
						// Scale by amount of characters skipped
						scoreInOrder += baseScore / ( 1 + j - jCarret );
						jCarret = j+1;
						break;
					}
				}
			}
			return scoreInOrder / term.Length;
		}





		public void Append( params string[] paths ) => Append( (IReadOnlyList<string>)paths );
		public void Append( params (string, int)[] pathsAndScore ) => Append( (IReadOnlyList<(string, int)>)pathsAndScore );
		public void Append( IReadOnlyList<string> paths )
		{
			bool changes = false;
			lock( _dictionary )
			{
				for( int i = 0; i < paths.Count; i++ )
				{
					string path = paths[ i ];
					if( _dictionary.TryAdd( path, 0 ) )
					{
						changes = true;
						OnNewPath( path );
					}
				}
			}
			if( changes )
				ScheduleSaveToDisk();
		}
		
		
		public void Append( IReadOnlyList<(string, int)> pathsAndScore )
		{
			bool changes = false;
			lock( _dictionary )
			{
				for( int i = 0; i < pathsAndScore.Count; i++ )
				{
					var(path, score) = pathsAndScore[ i ];
					if( _dictionary.TryAdd( path, score ) == false )
					{
						changes = true;
						int previousScore = _dictionary[ path ];
						int newScore = Math.Max( previousScore, score );
						if( newScore == previousScore )
							continue;
						
						_dictionary[ path ] = newScore;
					}
					else
					{
						OnNewPath( path );
					}
				}
			}
			if( changes )
				ScheduleSaveToDisk();
		}



		void OnNewPath( string path )
		{
			lock( _dirWatcher )
			{
				var disk = Path.GetPathRoot( path );
				(int amountOfPath, FileSystemWatcher watcher)data;
				if( _dirWatcher.TryGetValue( disk, out var tempData ) )
				{
					data = tempData;
				}	
				else
				{
					FileSystemWatcher fsWatcher;
					try
					{
						fsWatcher = new FileSystemWatcher
						{
							NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime,
							Path = disk, 
							Filter = "*.exe",
							IncludeSubdirectories = true
						};
					}
					catch(ArgumentException ae) when (ae.ParamName == "Path") // The directory name '*' does not exist.
					{
						return;
					}

					

					fsWatcher.Created += ( s, a ) =>
					{
						if( a.ChangeType == WatcherChangeTypes.Created )
							Append( a.FullPath );
					};
					// The user should remove it himself instead of us doing so automatically
					//fsWatcher.Deleted += ( s, a ) => Remove( a.FullPath );
					fsWatcher.Renamed += ( s, a ) =>
					{
						// Here we can remove as the file is just moved
						if( TryGetExecutionOf( a.OldFullPath, out int exec ) )
						{
							Remove( a.OldFullPath );
							// Only add the path if a watcher is already working on that disk,
							// the user might not want to index that disk / have exes on that disk indexed
							if( RegisteredWatcher( a.FullPath ) )
								Append( ( a.FullPath, exec ) );
						}
					};
								
					fsWatcher.EnableRaisingEvents = true;
								
					data = ( 0, fsWatcher );
					_dirWatcher.Add( disk, default );
				}

				_dirWatcher[ disk ] = ( data.amountOfPath + 1, data.watcher );
			}
		}



		void OnRemovePath( string path )
		{
			lock( _dirWatcher )
			{
				var disk = Path.GetPathRoot( path );
				if( _dirWatcher.TryGetValue( disk, out var data ) )
				{
					data = ( data.amountOfPath - 1, data.watcher );
					if( data.amountOfPath <= 0 )
					{
						data.watcher.Dispose();
						_dirWatcher.Remove( disk );
					}
					else
						_dirWatcher[ disk ] = data;
				}
			}
		}



		bool RegisteredWatcher( string path )
		{
			lock( _dirWatcher )
			{
				var disk = Path.GetPathRoot( path );
				return _dirWatcher.ContainsKey( disk );
			}
		}



		public void Remove( params string[] paths )
		{
			bool changes = false;
			lock( _dictionary )
			{
				for( int i = 0; i < paths.Length; i++ )
				{
					string path = paths[ i ];
					if( _dictionary.Remove( path ) )
					{
						changes = true;
						OnRemovePath( path );
					}
				}
			}
			if( changes )
				ScheduleSaveToDisk();
		}



		public void CleanMissingExe()
		{
			bool changes = false;
			lock( _dictionary )
			{
				var paths = _dictionary.ToArray();
				foreach( var path in paths )
				{
					if( File.Exists( path.Key ) == false )
					{
						_dictionary.Remove( path.Key );
						changes = true;
						OnRemovePath( path.Key );
					}
				}
			}
			if( changes )
				ScheduleSaveToDisk();
		}



		public bool TryIncreaseExecutionOf( string path, int increaseBy )
		{
			lock( _dictionary )
			{
				if( _dictionary.TryGetValue( path, out var val ) )
				{
					_dictionary[ path ] = val + increaseBy;
					ScheduleSaveToDisk();
					return true;
				}
				else
					return false;
			}
		}



		public bool TryGetExecutionOf( string path, out int execution )
		{
			lock( _dictionary )
			{
				return _dictionary.TryGetValue( path, out execution );
			}
		}



		public bool AppendFromFile( string path )
		{
			FileInfo file = new FileInfo(path);
			if( file.Exists == false )
				return false;

			XmlDocument doc = new XmlDocument();
			doc.Load( path );
			List<(string path, int score)> containedData = new List<(string path, int score)>();
			foreach( XmlNode containerNode in doc.ChildNodes )
			{
				if( string.Equals( containerNode.Name, XmlContainer, StringComparison.Ordinal ) == false )
					continue;
				
				// We most likely will have exactly this amount of items, might as well prepare the array for that  
				containedData.Capacity = Math.Max( containedData.Capacity, containedData.Count + containerNode.ChildNodes.Count );
				foreach( XmlNode childNodes in containerNode.ChildNodes )
				{
					if( string.Equals( childNodes.Name, XmlElement ) == false )
						continue;

					string elemPath = childNodes.Attributes[ XmlPath ]?.Value;
					if( string.IsNullOrEmpty( elemPath ) )
						continue;
					
					int score;
					string scoreStr = childNodes.Attributes[ XmlScore ]?.Value;
					if( scoreStr != null && int.TryParse( scoreStr, out var parsedScore  ) )
						score = parsedScore;
					else
						score = 0;

					containedData.Add( ( elemPath, score ) );
				}
			}

			Append( containedData );
			ScheduleSaveToDisk();
			return true;
		}




		void ScheduleSaveToDisk()
		{
			lock( _schedulerLock )
			{
				LastChangeTimeStamp = Stopwatch.GetTimestamp();
				if( _saveScheduled )
					return;
				
				_saveScheduled = true;
				DelayedSaveToDisk( this );
			}


			static async void DelayedSaveToDisk( Indexer indexer )
			{
				await Task.Delay( WRITE_TO_FILE_DELAY );
				indexer.ForceSaveToDisk();
			}
		}



		void ForceSaveToDisk()
		{
			KeyValuePair<string, int>[] dicContent;
			long timestamp;
			lock( _schedulerLock )
			{
				lock( _dictionary )
				{
					timestamp = System.Diagnostics.Stopwatch.GetTimestamp();
					dicContent = _dictionary.ToArray( );
				}
				_saveScheduled = false;
			}
				
			object[] xmlContent = new object[ dicContent.Length ];
			int i = 0;
			foreach( var kvp in dicContent.OrderByDescending( s => s.Value ) )
			{
				var score = kvp.Value;
				var path = kvp.Key;
				xmlContent[ i++ ] = new XElement( XmlElement,
					new XAttribute( XmlPath, path ),
					new XAttribute( XmlScore, score.ToString() )
				);
			}

			XElement root = new XElement( XmlContainer, xmlContent );

			lock( _writingToFileLock )
			{
				if( timestamp < _lastWriteTS )
					return; // Serialization process took too long, more recent data has already been written 
				_lastWriteTS = timestamp;
				
				var tempPath = $"{_IOPath}.tmp";

				byte[] output;
				using(var ms = new MemoryStream())
				{
					root.Save( ms );
					output = ms.ToArray();
				}
				using (var fs = new FileStream(tempPath, FileMode.OpenOrCreate))
				{
					fs.Write(output);
					fs.Flush();
				}
				// Writing to temporary path then overwrite actual path to avoid partial data
				// when application crashes while writing to disk.
				while (true)
				{
					try
					{
						File.Replace( tempPath, _IOPath, $"{_IOPath}.backup" );
						break;
					}
					catch (FileNotFoundException)
					{
						File.WriteAllBytes( _IOPath, new byte[0]);
					}
				}
			}
		}
		
		
		
		
		
		
		
		

		public static void TraverseRecursive( string path, ProgressionData progressionData, ref int totalSize, ref Node<string> currentNode )
		{
			try
			{
				foreach( var executable in Directory.EnumerateFiles( path, "*.exe", SearchOption.TopDirectoryOnly ) )
				{
					totalSize += 1;
					currentNode.Val = executable;
					currentNode = currentNode.Next = new Node<string>();

					progressionData.Current = executable;
					progressionData.AmountOfExe = totalSize;
				}
			}
			catch( UnauthorizedAccessException ) {}
			catch( PathTooLongException ) {}
			catch( DirectoryNotFoundException ) {}

			try
			{
				foreach( var subDir in Directory.EnumerateDirectories( path ) )
				{
					#warning Probably windows only
					if( subDir.EndsWith( "\\$RECYCLE.BIN", StringComparison.OrdinalIgnoreCase ) )
						continue;
					progressionData.Current = subDir;
					TraverseRecursive( subDir, progressionData, ref totalSize, ref currentNode );
				}
			}
			catch( UnauthorizedAccessException ) {}
			catch( PathTooLongException ) {}
			catch( DirectoryNotFoundException ) {}
		}
		
		public static string[] TraverseAndFetch( ProgressionData progressionData, params string[] pathsToProcess )
		{
			Node<string> rootNode = new Node<string>();
			int totalSize = 0;
			{
				Node<string> currentNode = rootNode;
				foreach( string root in pathsToProcess )
				{
					TraverseRecursive( root, progressionData, ref totalSize, ref currentNode );
				}
			}

			string[] final;
			{
				List<string> _temp = new List<string> { Capacity = totalSize };
				Node<string> currentNode = rootNode;
				while( currentNode.Val != null )
				{
					_temp.Add( currentNode.Val );
					currentNode = currentNode.Next;
				}

				final = _temp.ToArray();
			}
			
			progressionData.Done = true;
			return final;
		}



		public class ProgressionData
		{
			public string Current;
			public int AmountOfExe;
			public bool Done;
		}
		
		public class Node<T>
		{
			public T Val;
			public Node<T> Next;
		}
	}
}