namespace SAGAS3
{
	using System;
	using System.IO;
	using System.Threading;
	using Avalonia;
	using Avalonia.Controls;
	using Avalonia.ThemeManager;
	using Avalonia.Threading;



	internal static class Program
	{
		public static ThemeSelector Selector;
		static MainWindow _mainWindow;
		const string ExceptionFile = "ExceptionOutput.txt";



		// Initialization code. Don't use any Avalonia, third-party APIs or any
		// SynchronizationContext-reliant code before AppMain is called: things aren't initialized
		// yet and stuff might break.
		public static void Main( string[] args )
		{
			AppDomain.CurrentDomain.UnhandledException += ( sender, eventArgs ) => SendExceptionToLogOnly( eventArgs.ExceptionObject );
			if( File.Exists( ExceptionFile ) )
				File.WriteAllText( ExceptionFile, "" ); // Reset log file on start
			BuildAvaloniaApp().Start( AppMain, args );
		}



		// Avalonia configuration, don't remove; also used by visual designer.
		public static AppBuilder BuildAvaloniaApp()
			=> AppBuilder.Configure<App>()
				.UsePlatformDetect()
				.LogToTrace();



		static void AppMain( Application app, string[] args )
		{
			Selector = ThemeSelector.Create( "Themes" );
			try
			{
				var theme = Selector.LoadTheme( "Theme.xaml" );
				Selector.ApplyTheme( theme );
			}
			catch( Exception e )
			{
				SendExceptionToMessageBox( e );
			}

			_mainWindow = new MainWindow();
			AppDomain.CurrentDomain.UnhandledException += ( sender, eventArgs ) =>
			{
				SendExceptionToMessageBox( eventArgs.ExceptionObject as Exception );
			};
			// Dummy line to force static constructor
			var i = Indexer.Instance;
			app.Run( _mainWindow );
		}



		public static void SendExceptionToLogOnly( object e )
		{
			Console.WriteLine( e );
			while( true )
			{
				try
				{
					File.AppendAllText( ExceptionFile, $"{e}\n" );
					break;
				}
				catch( IOException ioex ) when (ioex.HResult == -2147024864) // Something is using that file, try again
				{
					Thread.Sleep(100);
					continue;
				}
			}
		}



		public static void SendExceptionToMessageBox( Exception e )
		{
			SendExceptionToLogOnly( e );
			if( Dispatcher.UIThread.CheckAccess() )
			{
				try
				{
					MessageBox.Show( _mainWindow, e?.ToString(), "Caught unhandled exception", MessageBox.MessageBoxButtons.Ok );
					return;
				}
				catch
				{
					// Doesn't matter a whole lot, we have redundancy
				}
			}

			try
			{
				Dispatcher.UIThread.Post( () =>
				{
					SendExceptionToMessageBox( e );
				} );
			}
			catch
			{
				// Doesn't matter a whole lot, we have redundancy
			}
		}
	}
}