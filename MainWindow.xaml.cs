namespace SAGAS3
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Avalonia;
    using Avalonia.Controls;
    using Avalonia.Input;
    using Avalonia.Layout;
    using Avalonia.Markup.Xaml;
    using Avalonia.Media;
    using Avalonia.Threading;
    using Color = Avalonia.Media.Color;



    public class MainWindow : Window
    {
        const int MAX_ITEM_COUNT = 80;
        public readonly LogicTheme LogicTheme = LogicTheme.Default;

        public readonly Canvas ContentCanvas;
        public readonly Canvas WindowCanvas;
        
        readonly ListBox _uiProgramList;
        readonly TextBlock _indexerText;
        readonly Button _startIndexer;
        public readonly TextBox UiSearchBar;
        Task _runningIndexerJob = Task.CompletedTask;

        object _searchLock = new object();
        CancellationTokenSource _cts = new CancellationTokenSource();
        (string term, long timestamp) _lastSearch = ( "", long.MinValue );
        
        public MainWindow()
        {
            Program.Selector.EnableThemes(this);
            
            AvaloniaXamlLoader.Load(this);
            Width = 300;
            Height = 400;
            SystemDecorations = SystemDecorations.BorderOnly;
            this.Background = LogicTheme.Background;

            
            
            this.ImplementDragFeature();
            
            
            UiSearchBar = new TextBox
            {
                Watermark = "...",
                FontSize = 18d,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = LogicTheme.Margin,
                Width = 206d,
                Height = 28d,
                AcceptsReturn = false,
                AcceptsTab = false,
                TextWrapping = TextWrapping.NoWrap,
                Padding = new Thickness( LogicTheme.Margin.Right, 0d ),
                BorderThickness = LogicTheme.Border,
            };
            UiSearchBar.KeyUp += ( s, a ) =>
            {
                if (a.Key == Key.Enter || a.Key == Key.Tab || a.Key == Key.Down)
                {
                    foreach (Control item in _uiProgramList.Items)
                    {
                        _uiProgramList.SelectedIndex = 0;
                        item.Focus();
                        break;
                    }
                    return;
                }

                Task.Run( () => SearchAndPopulate( UiSearchBar.Text ) ); 
            };
            UiSearchBar.GotFocus += (sender, args) =>
            {
                UiSearchBar.SelectionStart = 0;
                UiSearchBar.SelectionEnd = UiSearchBar.Text?.Length??0;
            };
            Dispatcher.UIThread.Post(() => { UiSearchBar.Focus(); });
            
            
            
            Button close = new Button
            {
                Width = 32d,
                Height = 22d,
                BorderThickness = LogicTheme.Border,
                Margin = new Thickness( LogicTheme.Margin.Right, 0d  ),
                Padding = new Thickness( 0d ),
                Background = LogicTheme.Red,
                Content = new TextBlock
                {
                    Text = "X",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                },
            };
            close.Click += ( s, e ) => Close();
            close.ImplementHighlight();
            
            Button minimize = new Button
            {
                Width = close.Width,
                Height = close.Height,
                BorderThickness = LogicTheme.Border,
                Margin = close.Margin,
                Padding = close.Padding,
                Background = new SolidColorBrush( Color.FromRgb( 80, 80, 80 ) ),
                Content = new TextBlock
                {
                    Text = "_",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                }
            };
            minimize.Click += ( s, e ) => WindowState = WindowState.Minimized;
            minimize.ImplementHighlight();
            
            
            
            
            _startIndexer = new Button
            {
                Width = Width * 0.66d - ( LogicTheme.Margin.Left ),
                Margin = LogicTheme.Margin,
                Height = 28d,
                BorderThickness = LogicTheme.Border,
                Padding = close.Padding,
                Background = LogicTheme.Blue,
                Content = _indexerText = new TextBlock
                {
                    Text = "Create Index",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                }
            };
            _startIndexer.Click += ( s, e ) => StartIndexer();
            _startIndexer.ImplementHighlight();


            var clearMissingFiles = new Button
            {
                Width = Width * 0.33d - ( LogicTheme.Margin.Right ),
                Margin = LogicTheme.Margin,
                Height = 28d,
                BorderThickness = LogicTheme.Border,
                Padding = close.Padding,
                Background = LogicTheme.Red,
                Content = new TextBlock
                {
                    Text = "Clean Missing",
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                }
            };
            clearMissingFiles.Click += ( sender, args ) =>
            {
                Task.Run(() => Indexer.Instance.CleanMissingExe());
            };
            clearMissingFiles.ImplementHighlight();
            
            
            _uiProgramList = new ListBox
            {
                Margin = LogicTheme.Margin,
                Width = Width - ( LogicTheme.Margin.Left + LogicTheme.Margin.Right ),
                Height = Height - ( _startIndexer.TotalDimensions().y + UiSearchBar.TotalDimensions().y ),
                BorderThickness = LogicTheme.Border,
                VirtualizationMode = ItemVirtualizationMode.None,
                Items = new []
                {
                    new ListBoxItem
                    {
                        Name = "Indexer",
                        Background = LogicTheme.Transparent,
                        Content = new TextBlock
                        {
                            Text = "Indexer has not been created/loaded yet",
                            Background = LogicTheme.Transparent,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center,
                        },
                    },
                }
            };
            
            ContentCanvas = new Canvas
            {
                Width = this.Width,
                Height = this.Height,
                Children =
                {
                    UiSearchBar,
                    _uiProgramList,
                    _startIndexer,
                    clearMissingFiles
                }
            };
            Content = WindowCanvas = new Canvas
            {
                Width = this.Width,
                Height = this.Height,
                Children =
                {
                    ContentCanvas,
                    close,
                    minimize
                }
            };
            Canvas.SetRight( close, 0d );
            Canvas.SetRight( minimize, close.Width );
            Canvas.SetTop( _uiProgramList, UiSearchBar.Height + (UiSearchBar.Margin.Bottom + UiSearchBar.Margin.Top) * 0.5d );
            Canvas.SetBottom( _startIndexer, 0 );
            Canvas.SetBottom( clearMissingFiles, 0 );
            Canvas.SetRight( clearMissingFiles, 0 );

            PopulateListWhenIndexerLoaded();
        }



        async void PopulateListWhenIndexerLoaded()
        {
            await Indexer.Instance.Loading;
            SearchAndPopulate( "" );
        }






        public async void SearchAndPopulate( string search )
        {
            CancellationTokenSource newCts;
            CancellationToken token;
            
            lock( _searchLock )
            {
                if( string.Equals( _lastSearch.term, search, StringComparison.Ordinal ) && _lastSearch.timestamp == Indexer.Instance.LastChangeTimeStamp )
                    return;
                _lastSearch = ( search, Indexer.Instance.LastChangeTimeStamp );
                newCts = new CancellationTokenSource();
                token = newCts.Token;
                _cts.Cancel();
                _cts = newCts;
            }

            var searchResult = Indexer.Instance.Search( search, token, false );

            if( token.IsCancellationRequested )
                return;
            
            if( searchResult.Length == 0 )
            {
                Dispatcher.UIThread.Post( () =>
                {
                    _uiProgramList.Items = new[]
                    {
                        new ListBoxItem
                        {
                            Name = "Indexer",
                            Background = LogicTheme.Transparent,
                            Content = new TextBlock
                            {
                                Text = "Indexer has not been created/loaded yet",
                                Background = LogicTheme.Transparent,
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                            },
                        },
                    };
                } );
                return;
            }
            
            int max = MAX_ITEM_COUNT > searchResult.Length ? searchResult.Length : MAX_ITEM_COUNT;
            
            await Dispatcher.UIThread.InvokeAsync( () =>
            {
                if( token.IsCancellationRequested )
                    return;
                if( _uiProgramList.Items is ObservableCollection<Control> c )
                {
                    foreach( Control control in c )
                    {
                        ShortcutItem val;
                        lock(ShortcutItem.UIToCache)
                            ShortcutItem.UIToCache.TryGetValue( control as ListBoxItem, out val );
                        lock(ShortcutItem.Cache)
                            ShortcutItem.Cache.Enqueue( val );
                    }
                    c.Clear();
                }
                else
                {
                    _uiProgramList.Items = new ObservableCollection<Control>();
                }
            } );
            
            for( int i = 0; i < max; i++ )
            {
                // We're kind of forced to delay and update piece wise to avoid blocking,
                // it'll take FAR more time to show the whole list but will feel better to the user
                try
                {
                    await Task.Delay( 10, token );
                }
                catch( TaskCanceledException )
                {
                    return;
                }
                await Dispatcher.UIThread.InvokeAsync( () =>
                {
                    if( token.IsCancellationRequested )
                        return;
                    
                    var path = searchResult[ i ].path;
                    var filename = Path.GetFileNameWithoutExtension( path );
                    if( string.IsNullOrWhiteSpace( path ) || string.IsNullOrWhiteSpace( filename ) )
                        return;
                    
                    ShortcutItem cachedItem;
                    lock( ShortcutItem.Cache )
                        ShortcutItem.Cache.TryDequeue( out cachedItem );
                    
                    cachedItem ??= new ShortcutItem( this, _uiProgramList.Width );
                    cachedItem.SetContent( path, filename );
                    
                    ((ObservableCollection<Control>)_uiProgramList.Items).Add(cachedItem.ListBoxItem);
                } );
            }
        }



        async void StartIndexer()
        {
            if( _runningIndexerJob.IsCompleted == false )
                return;

            string initialIndexerText = _indexerText.Text;
            var initialTextSize = _indexerText.FontSize;
            _startIndexer.IsEnabled = false;
            
            // Find drives to scan through
            var drives = Directory.GetLogicalDrives();
            drives = await IndexerPathPanel.AsModal( drives, this );
            var progressions = new Indexer.ProgressionData[ drives.Length ];
            for( int i = 0; i < progressions.Length; i++ )
                progressions[ i ] = new Indexer.ProgressionData();
            
            // Start a job to fetch files on this computer
            var task = Task.Run( async () =>
            {
                Task<string[]>[] tasks = new Task<string[]>[ drives.Length ];

                for( int i = 0; i < drives.Length; i++ )
                {
                    var drive = drives[ i ];
                    var progressionInstance = progressions[ i ];
                    tasks[ i ] = Task.Run( () =>
                    {
                        try
                        {
                            return Indexer.TraverseAndFetch( progressionInstance, drive );
                        }
                        catch( Exception e )
                        {
                            Program.SendExceptionToMessageBox( e );
                            progressionInstance.Done = true;
                            return Array.Empty<string>();
                        }
                    } );
                }

                int total = 0;
                foreach( var task in tasks )
                {
                    await task;
                    total += task.Result.Length;
                }

                List<string> allExe = new List<string>( total );
                foreach( var task in tasks )
                    allExe.AddRange( task.Result );
                
                return allExe.ToArray();
            } );
            _runningIndexerJob = task;
            
            
            // Show progression information on the UI
            var uiUpdate = Task.Run( async () =>
            {
                int done;
                do
                {
                    done = 0;
                    int total = 0;
                    string example = null;
                    foreach( Indexer.ProgressionData progressionInstance in progressions )
                    {
                        done += progressionInstance.Done ? 1 : 0;
                        total += progressionInstance.AmountOfExe;
                        if( progressionInstance.Done == false )
                            example = progressionInstance.Current;
                    }

                    await Dispatcher.UIThread.InvokeAsync( () =>
                    {
                        _indexerText.FontSize = initialTextSize * 0.9d;
                        string exampleShortened = example;
                        if( exampleShortened?.Length > 27 )
                            exampleShortened = $"{exampleShortened[ .. 12 ]}...{exampleShortened[ ^12 .. ]}";
                        _indexerText.Text = $"{exampleShortened} (drive:{done}/{progressions.Length})";
                    } );
                    
                    Thread.Sleep( 100 );
                } while( done != progressions.Length );
            } );
            
            
            
            await task;
            await uiUpdate;
            _startIndexer.IsEnabled = true;
            _indexerText.Text = initialIndexerText;
            _indexerText.FontSize = initialTextSize;
            Indexer.Instance.Append( task.Result );
            Dispatcher.UIThread.Post( () => SearchAndPopulate( "" ) );
        }
    }
}