﻿namespace SAGAS3
{
	using System.Diagnostics;
	using System.Threading;
	using System.Threading.Tasks;
	using Avalonia.Controls;
	using Avalonia.Threading;



	public abstract class MainWindowPanel
	{
		public readonly MainWindow MainWindow;
		protected abstract Task Job{ get; }
		protected abstract Canvas Canvas{ get; }



		protected MainWindowPanel( MainWindow mainWindow )
		{
			MainWindow = mainWindow;
			Dispatcher.UIThread.Post( () =>
			{
				Canvas.SetLeft( Canvas, -MainWindow.Width );
				MainWindow.WindowCanvas.Children.Insert( 0, Canvas );
				SwapCanvasJob( Canvas, MainWindow.ContentCanvas, MainWindow.Width );
			} );
			
			WaitForJobFinished();

			async void WaitForJobFinished()
			{
				await Job;
				await SwapCanvasJob( MainWindow.ContentCanvas, Canvas, MainWindow.Width );
				Dispatcher.UIThread.Post( () =>
				{
					MainWindow.ContentCanvas.Children.Remove( Canvas );
				} );
			}
		}

		

		static async Task SwapCanvasJob( Canvas canvasIn, Canvas canvasOut, double width, double duration = 0.25d )
		{
			await Task.Run( async () =>
			{
				Stopwatch sw = Stopwatch.StartNew();
				while( true )
				{
					var result = await Dispatcher.UIThread.InvokeAsync( () =>
					{
						float t = duration <= 0d ? 1f : (float) ( sw.Elapsed.TotalSeconds / duration );
						t = t.EaseInOut();
						Canvas.SetLeft( canvasOut, t * width );
						Canvas.SetLeft( canvasIn, ( 1f - t ) * -width );
						return t >= 1f;
					} );
					if( result )
						return;
					Thread.Sleep( 1 );
				}
			} );
		}
		
	}
}