using Avalonia;
using Avalonia.Markup.Xaml;

namespace SAGAS3
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
   }
}