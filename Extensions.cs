﻿namespace SAGAS3
{
	using System;
	using Avalonia;
	using Avalonia.Controls;
	using Avalonia.Layout;
	using Avalonia.Media;



	public static class Extensions
	{
		public static (double x, double y) TotalDimensions( this Layoutable item )
		{
			return ( item.Width + item.Margin.Left + item.Margin.Right, item.Height + item.Margin.Top + item.Margin.Bottom );
		}



		public static void ImplementHighlight( this Button button, Color? highlightParam = null )
		{
			if( button.Background is SolidColorBrush scb )
			{
				SolidColorBrush newSCB = new SolidColorBrush{ Opacity = scb.Opacity };
				var initialColor = scb.Color;
				Color highlightColor;
				if( highlightParam.HasValue )
				{
					highlightColor = highlightParam.Value;
				}
				else
				{
					unsafe
					{
						const byte INCREASE = 20;
						uint total = initialColor.ToUint32();
						byte* p = (byte*)(& total);
						// Increase and clamp to 255
						for( int i = 0; i < 4; i++ )
							p[ i ] = (byte)(p[ i ] + INCREASE > 255 ? 255 : p[ i ] + INCREASE);
						highlightColor = Color.FromArgb( p[ 3 ], p[ 2 ], p[ 1 ], p[ 0 ] );
					}
				}

				newSCB.Color = highlightColor;
				button.PointerEnter += ( sender, args ) =>
				{
					button.Background = newSCB;
				};
				button.PointerLeave += ( sender, args ) =>
				{
					button.Background = scb;
				};
			}
			else
			{
				throw new NotImplementedException( button.Background?.GetType().ToString() );
			}
		}



		public static void ImplementDragFeature( this Window window )
		{
			var dragFeatureData = new DragFeatureData();
			window.PointerPressed += ( s, a ) => dragFeatureData.Drag = true;
			window.PointerReleased += ( s, a ) => dragFeatureData.Drag = false;
			window.PointerLeave += ( s, a ) => dragFeatureData.Drag = false;
			window.PointerMoved += ( s, a ) =>
			{
				var pointerPos = a.GetPosition( window );
				var windowPos = window.Position;
				if( dragFeatureData.Drag )
				{
					var diff = pointerPos - dragFeatureData.LastPointerPos;
					var roundedDown = new PixelPoint( (int) diff.X, (int) diff.Y );
					window.Position = new PixelPoint( windowPos.X + roundedDown.X, windowPos.Y + roundedDown.Y );
					// Compensate for position being relative to the thing we move
					pointerPos -= new Point( roundedDown.X, roundedDown.Y );
				}
				dragFeatureData.LastPointerPos = pointerPos;
			};
		}
		
		/// <summary>
		/// The returned value will be lower than the given time value at the start but it'll sync up towards the end
		/// </summary>
		/// <param name="time">Value between 0 (start) and 1 (end)</param>
		/// <param name="tightness">1.0^ = add more easing</param>
		public static float EaseIn( this in float time, in float tightness = 2f ) => MathF.Pow( time, tightness );

		/// <summary>
		/// The returned value will be the same as the given time value at the start but it'll slow down towards the end
		/// </summary>
		/// <param name="time">Value between 0 (start) and 1 (end)</param>
		/// <param name="tightness">1.0^ = add more easing</param>
		public static float EaseOut( this in float time, in float tightness = 2f ) => 1f - MathF.Pow( 1f - time, tightness );

		/// <summary>
		/// The returned value will be lower than the given time value at the start, the same in the middle and it'll slow down towards the end
		/// </summary>
		/// <param name="time">Value between 0 (start) and 1 (end)</param>
		/// <param name="tightness">1.0^ = add more easing</param>
		public static float EaseInOut( this in float time, in float tightness = 2f )
		{
			float t = time > 1f ? 1f : time < 0f ? 0f : time;
			t *= 2f;
			if( t < 1f )
				return t.EaseIn( tightness ) * 0.5f;
			else
				return( t - 1f ).EaseOut( tightness ) * 0.5f + 0.5f;
		}



		public class DragFeatureData
		{
			public bool Drag;
			public Point LastPointerPos;
		}
	}
}