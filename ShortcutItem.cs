﻿namespace SAGAS3
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Drawing.Imaging;
	using System.IO;
	using System.Runtime.CompilerServices;
	using System.Runtime.InteropServices;
	using System.Threading;
	using System.Threading.Tasks;
	using Avalonia;
	using Avalonia.Controls;
	using Avalonia.Input;
	using Avalonia.Interactivity;
	using Avalonia.Layout;
	using Avalonia.Media;
	using Avalonia.Media.Imaging;
	using Avalonia.Threading;



	public class ShortcutItem
	{
		public static ConditionalWeakTable<ListBoxItem, ShortcutItem> UIToCache = new();
		public static Queue<ShortcutItem> Cache = new Queue<ShortcutItem>();


		public readonly ListBoxItem ListBoxItem;

		MainWindow _mainWindow;
		Canvas _canvas;
		TextBlock _text;
		Button _iconButton;

		string _path;
		string _label;

		
		
		public ShortcutItem( MainWindow mainWindow, double width )
		{
			this._mainWindow = mainWindow;
			var theme = mainWindow.LogicTheme;
			Canvas textCanvas;
			ListBoxItem = new ListBoxItem
			{
				Margin = default,
				Padding = default,
				BorderThickness = default,
				Content = _canvas = new Canvas
				{
					Width = width - ( theme.Margin.Left + theme.Margin.Right + 2 ),
					Height = theme.ListItemHeight,
					Children =
					{
						( textCanvas = new Canvas
						{
							Width = width - ( theme.Margin.Left + theme.Margin.Right ) - theme.ListItemHeight,
							Height = theme.ListItemHeight,
							Children =
							{
								( _text = new TextBlock
								{
									Background = theme.Transparent,
									FontSize = theme.ListItemHeight * 0.65d,
									HorizontalAlignment = HorizontalAlignment.Left,
									VerticalAlignment = VerticalAlignment.Center,
									Margin = new Thickness(),
								} )
							}
						} ),
						( _iconButton = new Button
						{
							Margin = new Thickness(),
							Height = theme.ListItemHeight,
							Width = theme.ListItemHeight,
							BorderThickness = theme.Border,
						} ),
					}
				}
			};
			lock( UIToCache )
				UIToCache.Add( ListBoxItem, this );

			Canvas.SetTop( _iconButton, 0 );
			Canvas.SetLeft( _iconButton, 0 );
			Canvas.SetTop( textCanvas, 0 );
			Canvas.SetLeft( textCanvas, _canvas.Height );
			Canvas.SetTop( _text, 0 );
			Canvas.SetLeft( _text, 0 );


			_iconButton.Click += IconButtonOnClick;
			ListBoxItem.DoubleTapped += ( _, _ ) => StartProcess();
			ListBoxItem.PointerPressed += ( sender, args ) =>
			{
				if( args.GetCurrentPoint( ListBoxItem ).Properties.IsRightButtonPressed )
					OpenExplorerAtPath();
			};
			ListBoxItem.KeyDown += ( sender, args ) =>
			{
				if( args.Key == Key.Enter )
					StartProcess();
				else if( ( args.Key >= Key.D0 && args.Key <= Key.Z ) || args.Key == Key.Back )
					mainWindow.UiSearchBar.Focus();
			};

			// When the name overflows from its container move it around inside 
			ListBoxItem.PointerEnter += ( s, a ) =>
			{
				double textWidth = _text.DesiredSize.Width;
				double canvasWidth = textCanvas.DesiredSize.Width;
				if( textWidth < canvasWidth )
					return;

				Task.Run( async () =>
				{
					Stopwatch sw = Stopwatch.StartNew();
					bool stillHovering;
					do
					{
						stillHovering = await Dispatcher.UIThread.InvokeAsync( () =>
						{
							if( ListBoxItem.IsPointerOver )
							{
								// Pingpong
								double widthDelta = _text.DesiredSize.Width - textCanvas.DesiredSize.Width;
								double time = sw.Elapsed.TotalSeconds / widthDelta * 20;
								double d = time % 1d;
								if( time % 2d > 1d )
									d = 1d - d;
								Canvas.SetLeft( _text, d * - widthDelta );
								return true;
							}
							else
							{
								// Set back to initial pos
								Canvas.SetLeft( _text, 0 );
								return false;
							}
						} );
						Thread.Sleep( 1 );
					} while( stillHovering );
				} );
			};
		}



		public void SetContent( string newPath, string newLabel )
		{
			_path = newPath;
			_text.Text = newLabel;
			_label = newLabel;
			_canvas.Height = _mainWindow.LogicTheme.ListItemHeight;
			_iconButton.Content = null;
			_iconButton.Background = new SolidColorBrush( Color.FromArgb( 0, 0, 0, 0 ), 0d );

			Task.Run( () =>
			{
				Bitmap? finalBitmap = null;
				try
				{
					if( File.Exists( _path ) )
					{
						using MemoryStream memory = new MemoryStream();
						using var icon = System.Drawing.Icon.ExtractAssociatedIcon( _path );
						using var netBitmap = icon.ToBitmap();
						netBitmap.Save( memory, ImageFormat.Png );
						memory.Position = 0;
						finalBitmap = new Bitmap( memory );
					}
				}
				catch( Exception e )
				{
					// No need to bother user in such cases, let's just log silently to file
					Program.SendExceptionToLogOnly( e );
				}

				try
				{
					Dispatcher.UIThread.Post( () =>
					{
						if( File.Exists( _path ) == false )
						{
							finalBitmap?.Dispose(); // If we had one but the file was removed in the mean time, clean it

							_text.Text = $"[FILE MISSING] {this._label}";
							_iconButton.Background = _mainWindow.LogicTheme.Red;
							_iconButton.Content = new TextBlock
							{
								Text = "X",
								HorizontalAlignment = HorizontalAlignment.Center,
								VerticalAlignment = VerticalAlignment.Center,
							};
						}
						else
						{
							Image image;
							_iconButton.Content = image = new Image
							{
								Opacity = 0.75d,
								Source = finalBitmap,
								Margin = new Thickness(),
								Height = _canvas.Height,
								Width = _canvas.Height,
							};
							image.DetachedFromVisualTree += ( _, _ ) => finalBitmap?.Dispose();
						}
						//iconButton.ImplementHighlight();
					} );
				}
				catch( Exception e )
				{
					Program.SendExceptionToMessageBox( e );
				}
			} );
		}



		void IconButtonOnClick( object? sender, RoutedEventArgs e )
		{
			if( File.Exists( _path ) )
				return;

			Indexer.Instance.Remove( _path );
			// No easy way to remove it directly from the view, this will do
			_canvas.Height = 0;
		}



		void OpenExplorerAtPath()
		{
			Task.Run( () =>
			{
				#warning implement 'open in explorer'-like behaviour for non-windows systems
				if( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) == false )
					return;

				// Find closest valid path when exe doesn't exist
				var validPath = Path.GetDirectoryName( _path );
				while( Directory.Exists( validPath ) == false && string.IsNullOrWhiteSpace( validPath ) == false )
					validPath = Path.GetDirectoryName( validPath );

				Process.Start( new ProcessStartInfo
				{
					FileName = "explorer.exe",
					Arguments = validPath ?? "",
					LoadUserProfile = false,
					//Verb = "runas", // UAC prompt
					UseShellExecute = false,
				} );
			} );
		}



		void StartProcess()
		{
			Task.Run( () =>
			{
				try
				{
					if( File.Exists( _path ) == false )
						return;
					Indexer.Instance.TryIncreaseExecutionOf( _path, 1 );
					Dispatcher.UIThread.InvokeAsync( () => _text.Text = $">{_label}..." );
					var info = new ProcessStartInfo
					{
						FileName = _path,
						LoadUserProfile = false,
						//Verb = "runas", // UAC prompt
						UseShellExecute = false, // Do not start process within this process tree
						WorkingDirectory = Path.GetDirectoryName( _path ),
					};
					try
					{
						Process.Start( info ).WaitForInputIdle();
					}
					catch( System.ComponentModel.Win32Exception e ) when( e.NativeErrorCode == 740 )
					{
						info.UseShellExecute = true; // must be true to respect verb
						info.Verb = "runas"; // Elevated, UAC prompt
						Process.Start( info ); // Can't wait for input with UseShellExecute
					}
				}
				catch( Exception e )
				{
					Program.SendExceptionToMessageBox( e );
				}
				finally
				{
					Dispatcher.UIThread.InvokeAsync( () => _text.Text = _label );
				}
			} );
		}

	}
}